package com.mycompany.app.mytests;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;

public class MyFirstTest {

    private String expectedText = "This is not an expected text";

    public MyFirstTest() {
        System.out.println("We are inside the constructor now!");
    }

    @Test
    public void MyFirstTestMethod() {
        System.out.println("We are inside the first test method");
    }

    @Test
    public void MySecondTestMethod() {
        System.out.println("We are inside the second test method");
        String visibleText = "This is not an expected text";
        assertThat("Expected text should be: " + expectedText + " but visible text is: " + visibleText, expectedText.equalsIgnoreCase(visibleText));
    }
}
