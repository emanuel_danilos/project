package com.mycompany.app.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GuruHomePage extends BasePage {

    private static String URL = "http://demo.guru99.com/test/newtours/index.php";

    @FindBy(how = How.NAME, using = "userName")
    WebElement userNameField;

    @FindBy(how = How.NAME, using = "submit")
    WebElement submitButton;

    @FindBy(how = How.LINK_TEXT, using = "SIGN-OFF")
    WebElement signOffButton;

    public void openHomePage() {
        driver.get(URL);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(userNameField));
    }

    public void clickSubmitBuuton() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(submitButton));
        submitButton.click();
    }

    public void loginSuccessfullyPageIsOpened() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(signOffButton));

    }

}
