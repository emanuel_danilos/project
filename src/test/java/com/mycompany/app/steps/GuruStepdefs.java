package com.mycompany.app.steps;

import com.mycompany.app.pages.GuruHomePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GuruStepdefs {

    GuruHomePage guruPage = new GuruHomePage();

    @Given("Main page is opened")
    public void mainPageIsOpened() {
        guruPage.openHomePage();
    }

    @When("User clicks Submit button")
    public void userClicksSubmitButton() {
        guruPage.clickSubmitBuuton();
    }

    @Then("Login Successfully page is opened")
    public void loginSuccessfullyPageIsOpened() {
        guruPage.loginSuccessfullyPageIsOpened();
    }
}
