package com.mycompany.app.steps;

import com.mycompany.app.pages.GooglePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GoogleStepdefs {

    GooglePage googlePage = new GooglePage();

    @Given("Google page is opened")
    public void googlePageIsOpened() {
    googlePage.openGooglePage();
    }

    @When("I close Cookie frame")
    public void iCloseCookieFrame() {
    googlePage.closeFrame();
    }

    @And("I search for phrase {string}")
    public void iSearchForPhraseStarWars(String searchPhrase) {
    googlePage.searchInGoogle(searchPhrase);
    }

    @Then("I can see search result page")
    public void iCanSeeSearchResultPage() {
        googlePage.verifyThatSearchResultsAreVisible();
    }
}
