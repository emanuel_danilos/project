Feature: Registration process

  Background:
    Given Home page is opened
    When User clicks Sign in link in top menu

  Scenario: Authentication page visibility
    Then Authentication page is opened

  Scenario Outline: Invalid registration process
    And User fill email address field with "<email>"
    And User clicks Create an account button
    Then User gets a message "<expectedMessage>"
    Examples:
      | email              | expectedMessage                                                                                                      |
      |                    | Invalid email address.                                                                                               |
      | wp@wp              | Invalid email address.                                                                                               |
      | test.test@test.com | An account using this email address has already been registered. Please enter a valid password or request a new one. |
