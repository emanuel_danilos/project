Feature: Sign in to shop

  Background:
    Given Home page is opened
    When User clicks Sign in link in top menu
    Then Authentication page is opened

  Scenario Outline: Authentication - incorrect values
    When User fill login field "<emailAddress>"
    And User fill password field "<password>"
    And User click Sign in button
    Then Error message is visible "<errorMessage>"
    Examples:
      | emailAddress  | password | errorMessage               |
      |               |          | An email address required. |
      | invalid email | test     | Invalid email address.     |
      | test_test.pl  | test     | Invalid email address.     |
      | test@test.pl  |          | Password is required.      |
      | test@test.pl  | 1        | Invalid password.          |
