Feature: Google search feature

  Background:
    Given Google page is opened

  Scenario Outline: Search in Google
    When I close Cookie frame
    And I search for phrase "<searchPhrase>"
    Then I can see search result page
    Examples:
      | searchPhrase   |
      | Star Wars      |
      | Luke Skywalker |
      | Obi Wan Kenobi |

#  Scenario Outline: Search in Google without closing Cookie frame
#    And I search for phrase <searchPhrase>
#    Then I can see search result page
#    Examples:
#      | searchPhrase    |
#      | "Clone trooper" |
